# -*- coding: utf-8 -*-
"""
Created on Wed May 24 12:54:08 2017

@author: uyat
"""

import pandas as pd
csv_file_name = 'movie-pang02.csv'
csv_df = pd.read_csv(csv_file_name)
from nltk.stem.snowball import SnowballStemmer

def preprocessText(data_df,stopword_df):
    stemmer = SnowballStemmer('english')
    stopword_list = stopword_df['col1'].values.tolist()
    temp_text = data_df['text']
    res_sentence=[]
    for x in temp_text:
        resultwords = {stemmer.stem(word) for word in x.split() if word.lower() not in stopword_list}
        #resultwords = {stemmer.stem(word) for word in resultwords }
        res = ' '.join(resultwords)
        res_sentence.append(res)
    #print(res_sentence)
    data_df['text'] = res_sentence        
    print(data_df['text'])
    return data_df
sw = ['the','with','surgeon']
stopword_df = pd.DataFrame(sw,columns=['col1'])   
print(stopword_df)     
preprocessText(csv_df,stopword_df)
